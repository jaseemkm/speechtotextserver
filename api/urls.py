from django.conf.urls import url

from .views import SpeecToText

urlpatterns = [
    url(r'^speech-to-text',
        view=SpeecToText.as_view(),
        name="speech_to_text"),

]