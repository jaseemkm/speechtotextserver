from rest_framework.views import APIView
from rest_framework import status
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.response import Response
import speech
from pydub import AudioSegment
from django.core.files.storage import FileSystemStorage

# Create your views here.


class SpeecToText(APIView):


    def post(self, request, *args):
        detect = speech.Recognizer()
        file = request.data['fileToUpload']
        mp4_version = AudioSegment.from_file(file, "mp4")
        file = mp4_version.export('speech.wav', format="wav")
        print(file)
        audioFile = speech.AudioFile('speech.wav')
        with audioFile as source:
            audio = detect.record(source)
        try:
            text = detect.recognize_sphinx(audio)
        except speech.UnknownValueError:
            text = "Oops! Didn't catch that"
        except speech.RequestError as e:
            text = "Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e)
        response = {
            "converted_data": text
        }
        return Response(response, status=status.HTTP_200_OK)